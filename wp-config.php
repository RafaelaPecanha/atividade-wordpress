<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wp_db' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8-nLWttY^ev tt]O/rr]t1h-k0G%@; `uh8}.&v/1j!,W,=QVfTBr7>sIKR,5lQ*' );
define( 'SECURE_AUTH_KEY',  'ji5|)ZY?zPWOM9BkNQV^=LnG$p{0$O)Nm]#)uwKh4F!J) 0(MImyF4L2WOkf_xTW' );
define( 'LOGGED_IN_KEY',    '2Kf%NnJc^[!ubBFU 7qc{X6b&[q1QApZH}F+@X5EuRh`wXD!(LP))m{k^G~xKZpG' );
define( 'NONCE_KEY',        'lU^E^vhqdO8^}:g6Ok>=pU/Q5W)}-|]ixF|kOYz=V%akALTV)@bYYC`9fWh{xqBh' );
define( 'AUTH_SALT',        '7_7l4&8wc.PH?Yh.9}h=u%G{|ihN7T.B;:r)}o(ad JN5]X6F;Ha(7#_C9zYZdM8' );
define( 'SECURE_AUTH_SALT', 'sD.Ast{}WUeRA<bKX.?wGS1gmFK{45H98Q2*/iMH,Fw uWc|D__.=DxLSELw8 c6' );
define( 'LOGGED_IN_SALT',   '~pK[c/lPW1er5J=;A.;:aAf{zh]^iL8Th@qyh!Qddhii#4Z,zp:0DWr9|!g:)~n ' );
define( 'NONCE_SALT',       'Nz?[5*bobtBS}f(#4/KjJa?|PQC}+MAr>K4WY${P|E?ALMH*1c=IL Ofq{n#tR =' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
