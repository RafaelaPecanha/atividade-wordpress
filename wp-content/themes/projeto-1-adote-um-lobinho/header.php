<!-- https://www.figma.com/file/xEHjDknJIBP9CKfAlOv1Yt/Adote-um-Lobinho-2021.3-(Copy) - Figma do projeto -->

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name') ?> </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--Fonte dos links do topo-->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    
    <?php wp_head(); ?>

</head>
<body>
    <section>
        <header class="menu-topo">
            <p class="links-topo"><a href="busca.html">Nossos Lobinhos</a></p>
            <a href="#"><img class="image" src="<?php echo get_stylesheet_directory_uri() ?>/Adote um Lobinho 2021.3 (Copy)/slogan.png" alt="slogan"  id="teste"></a>
            <div>
                <p ><a class="links-topo2 nodecoration" href="Layout-Quem_somos.html">Quem Somos</a></p>
                <h2><a class = "seta nodecoration" href="Layout-Quem_somos.html">Somos lob{IN}hos!</a></h2>
            </div>

        </header>