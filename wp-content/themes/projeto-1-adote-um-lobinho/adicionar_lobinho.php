<?php
// Template Name: adicionar lobinho
?>
    
    <?php get_header(); ?>
        <main>
            
        <section class = "painel">
            <section class = "container">
                <h3><?php the_field('titulo'); ?></h3>
                    <section id="container-text">
                       
                            <p><?php the_field('nome'); ?></p>
                            <textarea class ="messagem" id="name" rows="1" cols = "50" required="required" minlength="4" maxlength="60" placeholder="Digite o nome ..."></textarea>
                            <p><?php the_field('anos'); ?></p>
                            <textarea class ="messagem" id="age" rows="1" cols = "20" required="required" minlength="0" maxlength="100" placeholder="Ano..."></textarea>
                            <p><?php the_field('link'); ?></p>
                            <textarea class ="messagem" id="photo" rows="1"  cols = "80" required="required" placeholder="Digite o link..."></textarea>
                            <p><?php the_field('descricao'); ?></p>
                            <textarea class ="messagem" id="descrip" rows="10" cols = "80" required="required" minlength="10" maxlength="255"placeholder="Digite a descrição..."></textarea>
                            <button class="botao2">Salvar</button>
                     
                        
                    </section> 
            </section>
        </section>
            
        </main>
<?php get_footer(); ?> 