const url = "https://lobinhos.herokuapp.com" //Link da API

const screenDiv = document.querySelector('.adopt')

const wolfMaker = (wolf) => {
    const photo = wolf.image_url
    const descrip = wolf.description
    const name = wolf.name
    const id = wolf.id
    const wolfDiv = document.createElement('div')
    wolfDiv.innerHTML = `
    <div class="title">
        <h1>${name}</h1>
    </div>
    <div class="main flex">
        <div class="content1 flex">
            <div class="imagem">
                <img src="${photo}" alt="Foto do lobinho ${name}">             
            </div>
            <div class="button flex">
                <div class="button1">
                    <a href="Layout-Adotar_lobinho.html?id=${id}" target="blank">adotar</a>
                </div>
                <div class="button2">
                    <a href="#">excluir</a>
                </div>
            </div>
        </div>
        <div class="content2 flex">
            <p class="texto1">${descrip}</p>
        </div>    
    </div>
    `
   screenDiv.appendChild(wolfDiv)
}

const getThatWolf = () => {
    const urlParam = new URLSearchParams(window.location.search).get('id')
    fetch(url + "/wolves/" + urlParam)
    .then(response => response.json())
    .then(wolf => {
        wolfMaker(wolf)
    })
    .catch(error => {
        console.error(error)
    })
}

getThatWolf()