const url = "https://lobinhos.herokuapp.com" //Link da API

const wolfAdopt = (wolf) => {
    //Retorno das informações do adotante
    const adopterName = document.querySelector('.inputmaior').value
    const adopterMail = document.querySelector('.inputmenor').value
    const adopterAge = document.querySelector('.inputmaior2').value

    wolf.adopter_name = adopterName
    wolf.adopter_email = adopterMail
    wolf.adopter_age = adopterAge
    wolf.adopted = true

}

const getThatWolf = () => {
    const urlParam = new URLSearchParams(window.location.search).get('id')
    fetch(url + "/wolves/" + urlParam)
    .then(response => response.json())
    .then(wolf => {
        wolfAdopt(wolf)
    })
    .catch(error => {
        console.error(error)
    })
}

const postThatWolf = () => {
    
}

const btnAdocao = document.querySelector('.adoption')
btnAdocao.addEventListener("click", getThatWolf)