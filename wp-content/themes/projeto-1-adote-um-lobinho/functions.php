<?php
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'start_post_rel_link', 10, 0 );
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');



    
    
    function nomeescolhido(){
        wp_enqueue_style( 'reset.css', get_template_directory_uri() . '/css/reset.css');
        wp_enqueue_style( 'style.css', get_template_directory_uri() . '/style.css');
       wp_enqueue_style( 'adicionar_lobinhos.css', get_template_directory_uri() . '/css/adicionar_lobinhos.css');
       wp_enqueue_style( 'busca.css', get_template_directory_uri() . '/css/busca.css');
       wp_enqueue_style( 'Layout-Adotar_lobinho.css', get_template_directory_uri() . '/css/Layout-Adotar_lobinho.css');
        wp_enqueue_style( 'Layout-Quem_somos.css', get_template_directory_uri() . '/css/Layout-Quem_somos.css');
        wp_enqueue_style( 'Layout-Show_Lobinho.css', get_template_directory_uri() . '/css/Layout-Show_Lobinho.css');

    }

    add_action('wp_enqueue_style','nomeescolhido');

    // if(is_page_template (Layout-Quem_somos)){
    //     function csslobinho(){
    //         wp_enqueue_style( 'Layout-Show_Lobinho.css', get_template_directory_uri() . '/css/Layout-Show_Lobinho.css');

    //     }

    // }

    function my_theme_scripts_function() {
        wp_enqueue_script( 'index', get_template_directory_uri() . '/index.js');
        wp_enqueue_script( 'busca_lobinhos', get_template_directory_uri() . '/busca_lobinhos.js');
        wp_enqueue_script( 'add_lobinho', get_template_directory_uri() . '/add_lobinho.js');
        wp_enqueue_script( 'adotar', get_template_directory_uri() . '/adotar.js');
        wp_enqueue_script( 'show', get_template_directory_uri() . '/show.js');
        
      }
      add_action('wp_enqueue_scripts','my_theme_scripts_function');
?>